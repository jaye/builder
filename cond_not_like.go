// Copyright 2016 The Xorm Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package builder

import "fmt"

// NotLike defines like condition
type NotLike [2]string

var _ Cond = NotLike{"", ""}

// WriteTo write SQL to Writer
func (like NotLike) WriteTo(w Writer) error {
	if _, err := fmt.Fprintf(w, "%s NOT LIKE ?", like[0]); err != nil {
		return err
	}
	// FIXME: if use other regular express, this will be failed. but for compatible, keep this
	if like[1][0] == '%' || like[1][len(like[1])-1] == '%' {
		w.Append(like[1])
	} else {
		w.Append("%" + like[1] + "%")
	}
	return nil
}

// And implements And with other conditions
func (like NotLike) And(conds ...Cond) Cond {
	return And(like, And(conds...))
}

// Or implements Or with other conditions
func (like NotLike) Or(conds ...Cond) Cond {
	return Or(like, Or(conds...))
}

// IsValid tests if this condition is valid
func (like NotLike) IsValid() bool {
	return len(like[0]) > 0 && len(like[1]) > 0
}
